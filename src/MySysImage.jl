module MySysImage

import SnoopPrecompile

# data structures
import CoordinateTransformations
import DataFrames, DataFramesMeta
import FixedPointDecimals, StaticArrays
import Measurements, MonteCarloMeasurements
import Graphs, MetaGraphs
import Unitful, UnitfulUS

# file I/O
import FileIO
import Images, Luxor
import JSON, YAML
import JSON3, StructTypes
import Tables, CSV, XLSX
import MPI, HDF5 # HDF5 parallel requires MPI
import ZipFile

# general utilities
import ArgParse
import Dates
import FilePaths
import Parameters

# plots
import Crayons
import GR
import Plots, Plots.PlotMeasures, StatsPlots
import Interact, WebIO
import PlotlyJS, PlotlyBase

# domain specific
import Pluto
import Revise
import HTTP, Genie
import ControlSystemsBase, ControlSystems

# data ninja
#import Recombinase, Blink, Interact
import PyCall

# processing
import DSP, FFTW, MFCC, Wavelets
import Lazy
import Optim
import Interpolations



SnoopPrecompile.@precompile_all_calls begin
    df = DataFrames.DataFrame()
    Plots.gr(); Plots.plot([1,2,3.0], [4,5,6.0])
    #Plots.plotlyjs(); Plots.plot([1,2,3.0], [4,5,6.0])
    v = StaticArrays.SVector{3}([4,5,6])
    j = JSON.parse("""[{"foo": "bar", "qwerty": "aoeu"}, 324.0]""")
    j = YAML.load("""[{"foo": "bar", "qwerty": "aoeu"}, 324.0]""") # TODO better example

    Genie.route("/", () -> "Text")
    Genie.up(12344)
    HTTP.get("http://localhost:12344")
    Genie.down!()
    np = PyCall.pyimport("numpy")

    Interpolations.linear_interpolation([0.0, 1.0], [0.0, 10.0])(0.5)

    mktempdir() do dir
        f = joinpath(dir, "hdf.hdf")
        HDF5.h5open(f, "w") do hdf
            HDF5.create_group(hdf, "mygroup")
        end
        HDF5.h5open(f, "r") do hdf
            @show hdf
        end
    end

    fibs = Lazy.@lazy 0:1:(fibs + drop(1, fibs));

    tf = ControlSystems.tf([1.0], [2.0, 3.0])
end

end
