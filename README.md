# Alex's system image for Julia

This package creates and enables efficient use of a system image in Julia.

With Julia on your `$PATH`, type `make repl` (or just `make`) and let the magic happen.
In the background, the set of Julia packages listed in `Project.toml` will be installed, a driver script will be created, and a sysimage will be created using the pre-compilation statements in `src/MySysImage.jl`.

After it has been created, you can just add the `scripts` folder to `$PATH` to reap the benefits of your packages precompiled into Julia.

To update the sysimage, run `make update`, which issues `]update` and then recompiles the sysimage.

To use this in Jupyter, for now you will have to manually edit `kernel.json` to point to the `scripts/julia` script.
This does work, however!


## Caveats

There are problems using this sysimage for Jupyter (and potentially in other places) when packages such as `Gtk.jl` are compiled-in.
